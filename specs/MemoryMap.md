# x7cpu memory map

x7cpu makes a few assumptions about the system memory map.

Note the first three regions overlap. They are listed in order of precedence;
in the documentation for each region, the conditions causing a lookup to fall
through to the next overlapping region are described.

| From     | To       | Size   | Name              |
|---------:|---------:|-------:|-------------------|
| 00000000 | 00000007 |   8  B | `BootPath`        |
| 00000000 | 0000FFFF |  64 kB | `LocalArea`       |
| 00000000 | 1FFFFFFF | 512 MB | `Cpu0Mem`         |
| 20000000 | 3FFFFFFF | 512 MB | `Cpu1Mem`         |
| 40000000 | 5FFFFFFF | 512 MB | `Cpu2Mem`         |
| 60000000 | 7FFFFFFF | 512 MB | `Cpu3Mem`         |
| 80000000 | 80000007 |   8  B | `SysBootPath`     |

## `BootPath`

The `BootPath` region contains the first eight bytes of each CPU's local
physical address space. This is the location from which all m68k CPUs boot,
and it is given special treatment by the x7cpu bus controller:

- On CPU 0, `BootPath` is redirected to `SysBootPath`.
- On CPUs 1 through 3, `BootPath` falls through to `LocalArea`.

Thus to boot CPU 0, the bus controller redirects the code and stack pointer
lookups to `SysBootPath`, where a motherboard implementing x7cpu is expected
to provide a boot flash. To boot CPUs 1 through 3, the operating system kernel
running on CPU 0 initializes their boot pointers via the `Cpu1Mem`, `Cpu2Mem`,
and `Cpu3Mem` blocks, and the bus controller on those cards redirects the
lookups into these blocks.

## `LocalArea`

The `LocalArea` region comprises the first 64 kiB of physical address space.
This region is redirected by the bus controller into the first 64 kiB of
`Cpu0Mem`, `Cpu1Mem`, `Cpu2Mem`, or `Cpu3Mem` depending on the CPU index (on
CPU 0, this is a fallthrough to an overlapping region). This allows
processor-local information, including the vector table, to be stored per-CPU
starting at `00000000` and accessed from any CPU without knowledge of its ID.

Note that the memory map does not permit CPUs 1 through 3 to access CPU 0's
local area in any way (whereas 1-3's local areas can be accessed directly at
`20000000`, `40000000`, and `60000000`). It is recommended for the OS kernel to
perform initialization of all local blocks from CPU 0, and then for all CPUs to
access only their own local blocks from that point on.

## `Cpu0Mem`, `Cpu1Mem`, `Cpu2Mem`, `Cpu3Mem`

These regions correspond to the RAM installed on each CPU card. When a CPU
accesses its own region, the bus controller performs the access locally,
allowing fast local memory operations. If the address bits do not match the
local CPU index, a system bus transfer is performed instead; the motherboard's
bus controller is expected to facilitate the handshake to transfer this into
the correct CPU card.

## `SysBootPath`

The `SysBootPath` region is the address to which boot pointer lookups are
redirected on CPU 0. It is expected that the motherboard will map the boot
firmware flash starting at the beginning of `SysBootPath`. Note that only the
first eight bytes (code and stack pointers) are redirected, so the reset vector
must point directly into the boot flash starting at or after `80000000`.
